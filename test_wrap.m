clear
close all
lambda = 0.8;
bD = [-lambda 1];
aD = [1 -lambda];

Fs=48000;
Fn = Fs/2;   
nfft  =2^16;
f=(0:nfft/2-1)'*Fs/nfft;
Fprecision = Fs/nfft;



t = (0:2^16)/Fs;
t=t';
h0 = sin(2*3*400*t);
h0_wrap = filter(bD,aD,h0);


H0_wrap = fft(h0_wrap,nfft);
H0 = fft(h0,nfft);

figure();
subplot(2,1,1);
plot(t,h0);
subplot(2,1,2);
semilogx(f,20*log10(H0(1:nfft/2,1)),f,20*log10(H0_wrap(1:nfft/2,1)));
legend('Hmoy','wrap')