
clear; 
close all;

Fs=48000;
%t0=(0:size(h,1)-1)/Fs;
samples = 2^16;   % Window Length of FFT 
t=(0:samples-1)/Fs;
Fn = Fs/2;   
nfft = 2^nextpow2(samples);
f=(0:nfft/2-1)'*Fs/nfft;
Fprecision = Fs/nfft;

Fmin = 10;
Fmax = 30000;
Mmin = -10;
Mmax = 20;
SmoothMag = 101;
SmoothPh = 5;

offset = 0; %20/1000*Fs;        %number of samples
winLen1= floor(20/1000*Fs);
winLen2 = floor(200/1000*Fs);
winsize = floor(400/1000*Fs);

target_flag =1;        %0 = use target.wav as a target
                       %1 : use a default response as a target                      
mq = [ 12, 12,  12, 12, 10,   0,     0,     -6];
fq = [ 1, 15, 30, 60, 120, 1000, 10000, 20000];

ihlength = 2^13;
order_kirk = 6;
algo = 'kirk';

octSmooth = @iosr.dsp.smoothSpectrum;
order_smooth = 6;


%=============== Loading impulses from winMLS =============== %
%{
filenames = {'top1.wmb';'top2.wmb' ; ...
              'top3.wmb' ; 'top4.wmb';...
              'top5.wmb' ; 'top6.wmb' ; ...
              'top7.wmb' ; 'top1.wmb';
};
targetName = {'target.wmb'};

% Loading measures
[imps,Fs2, Format]=loadimp(char(filenames(1)));
for k=2:(length(filenames))
    imp=loadimp(char(filenames(k)));
    imps=[imps imp];
end
% Loading target
[target]=loadimp(char(targetName(1)));
%}

%============== Loading Impulses from wav files ===============%
%%{

filenames = { 'test1.wav' ; 'test2.wav' ; ...
              'test3.wav' ; 'test4.wav';...
              'test5.wav' ; 'test6.wav' ; ...
              'test7.wav' ; 'test8.wav';
};
targetName = {'target.wav'};

[imps,Fs2]=audioread(char(filenames(1)));
    
for k=2:length(filenames)
    imp=audioread(char(filenames(k)));
    imps=[imps imp];
end

[target]=audioread(char(targetName(1)));

%}

%=========== Initialisation ===============%
h = imps;
% Impulse number
nImp = length(filenames);

% For display
couleur = hsv(nImp+2);
legendeMag = cell(1,nImp);
legendeMes = cell(1,nImp/2);
for ii=1:(nImp/2)
    legendeMes(ii) = {sprintf('Mes %i',ii)};
end
for ii=1:nImp
   legendeMag(ii)=filenames(ii);
end
legendePh = legendeMag;

%========= Windowing ==============%
w1=hanning(2*winLen1);
w2=hanning(2*winLen2);
w=[w1(1:winLen1) ; ...
   ones(winsize,1); ...
   w2(winLen2:end) ; ...
   zeros(samples-winLen1-winLen2-winsize-1,1)];
h=h(1+offset:samples+offset,:);
target = target(1+offset:samples+offset);
hn=zeros(size(h));
for k=1:size(h,2);
    h(:,k) = h(:,k).*w;
    %hn(:,k)=h(:,k)/max(abs(h(:,k))); 
end
target = target.*w;

%Plot
figure('Name', 'Impulses and windowing')
subplot(2,1,1);
plot(t,w);
subplot(2,1,2);
for k=1:size(h,2);   
    plot(t,h(:,k));
    hold on;
end
drawnow;
%wvtool(w);
%wvtool(hanning(samples));

% =============== Compute FFTs ============== %
H=fft(h,nfft);
%%{
%Normalization
for i=1:size(H,2)
    m = sum(abs(H(:,i)),1) ./ size(H,1);
    H(:,i) =  H(:,i) ./ m ;
end
%}

%=============== Building target ====================%
if target_flag == 0
    Htarget = abs(fft(target,nfft));
else
    Htarget = interp1(log(fq),mq,log(f), 'linear','extrap');
    Htarget = 10.^(Htarget/20);

    % Uncomment for flat target
    %Htarget = zeros(size(f,1),1);  
end

% Smooting
Htarget = octSmooth(abs(Htarget(1:nfft/2)),f,order_smooth);
% Normalization
%{
m = sum(abs(Htarget),1) ./ size(Htarget,1);
Htarget =  Htarget ./ m;
%}

% Plot
figure();
semilogx(f,20*log10(Htarget));
axis([10 10000 -5 20]);
drawnow;


%================ Build prototype response ============%
%Compute mean value
Hmoy = sum( abs(H),2) / size(H,2);

%----- Preprocessing------%

%octave smoothing
%GENERALIZED FRACTIONAL OCTAVE SMOOTHING OF AUDIO / ACOUSTIC RESPONSES
Hmoy = octSmooth(abs(Hmoy(1:nfft/2)),f,order_smooth);

%------ Applying Target ------%
H0=Hmoy(1:nfft/2) ./ Htarget;
h0 = ifft(H0,nfft,'symmetric');

%{
%------ Frequency warping ---------%
%Frequency-Warped Signal Processing for Audio Applications
lambda = 0.8;
bD = [-lambda 1];
aD = [1 -lambda];

h0_wrap = filter(bD,aD,h0);

figure();
H0_wrap = fft(h0_wrap,nfft);
semilogx(f,20*log10(Hmoy(1:nfft/2,1)),f,20*log10(H0_wrap(1:nfft/2,1)));
legend('Hmoy','wrap')
%}

%================= Inversing =====================%

if strcmp(algo,'kirk')
    %Kirkeby%
    ih_kirk = invFIR('minphase',h0,nfft,order_kirk,ihlength,[30 12000], [50 0],0);
    IH_kirk = fft(ih_kirk,nfft);
elseif strcmp(algo,'lpc')
    %LPC Analysis
    [r,lg] = xcorr(h0,'biased');
    r(lg<0) = [];
    ih_lpc = levinson(r,ihlength);
    ih_lpc = ih_lpc';
    IH_lpc = fft(ih_lpc,nfft);
end

%================= Select the algorithm ===============%
if strcmp(algo,'kirk')
    ih0 = ih_kirk;
    IH0 = IH_kirk;
elseif strcmp(algo,'lpc')
    ih0 = ih_lpc;
    IH0 = IH_lpc;
end

% ================ Convolution =================%
for i=1:size(H,2)
    Y(:,i) = H(:,i) .* IH0;
end

Ymoy = sum( abs(Y) ,2) / size(Y,2);
Ymoy = octSmooth(abs(Ymoy(1:nfft/2)),f,order_smooth);

%================= Showing results ==================%
%-------Display filter ------%
figure('Name', strcat('Filter : ',algo));
subplot(3,1,1);
plot(1:length(ih0),ih0);
subplot(3,1,2);
semilogx(f,20*log10(abs(IH0(1:nfft/2))));
axis([Fmin Fmax Mmin Mmax]);
axis 'auto y';
subplot(3,1,3);
semilogx(f,angle(IH0(1:nfft/2)));

%--------Before--------%
%Print measures
figure('Name','Moyennes avant');
subplot(2,1,1);
semilogx(f,20*log10(smooth(abs(H(1:nfft/2,1)),SmoothMag)),...
             'color',couleur(1,:),'linewidth',1);
for k=2:size(H,2)
    hold on
    semilogx(f,20*log10(smooth(abs(H(1:nfft/2,k)),SmoothMag)),...
             'color',couleur(k,:),'linewidth',1);
end
%Print prototype
%%{
hold on
semilogx(f,20*log10(smooth(abs(Hmoy(1:nfft/2)),SmoothMag)),...
        'color',couleur(end-1,:),'linewidth',2.5); %}  
%}

%Print target
hold on
semilogx(f, 20*log10(smooth(abs(Htarget(1:nfft/2)),1)),...
        'color',couleur(1,:),'linewidth',2.5);
axis([Fmin Fmax Mmin Mmax]);
legende = [legendeMag, {'Prototype', 'Target'}];
legend(legende );

subplot(2,1,2);
%Print inverse filter
semilogx(f, 20*log10(smooth(abs(IH0(1:nfft/2)),SmoothMag)));

%Print prototype compensated response
hold on
semilogx(f, 20*log10(smooth(abs(H0(1:nfft/2)),SmoothMag)));

% Print Difference between representative and inverse
hold on
semilogx(f, 20*log10(smooth(abs(H0(1:nfft/2)) .* abs(IH0(1:nfft/2)),SmoothMag)));

axis([Fmin Fmax -20 20]);
legend({'Inverse', 'Prototype compensated','difference'});


%On recupere la bonne echelle de magnitude
%{
v = axis;
Mmin = v(3);
Mmax = v(4)+5;
%}
drawnow;
%-----------After----------%
%Print measures
figure('Name','Moyennes apres');
semilogx(f,20*log10(smooth(abs(Y(1:nfft/2,1)),SmoothMag)),...
             'color',couleur(1,:),'linewidth',1);
for k=2:size(H,2)
    hold on
    semilogx(f,20*log10(smooth(abs(Y(1:nfft/2,k)),SmoothMag)),...
             'color',couleur(k,:),'linewidth',1);
end
%Print mean
hold on
semilogx(f,20*log10(smooth(abs(Ymoy(1:nfft/2,1)),SmoothMag)),...
        'color',couleur(end-1,:),'linewidth',2.5); 

axis([Fmin Fmax Mmin Mmax]);
%axis 'auto y';

%Print target
hold on
semilogx(f,20*log10(smooth(abs(Htarget(1:nfft/2)),1)),...
         'linewidth',2);
     
%On recupere la bonne echelle de magnitude
%{
v = axis;
Mmin = v(3);
Mmax = v(4)+5;
%}
legende = [legendeMag, {'Mean', 'Target'}];
legend(legende);
drawnow;

%{
%====================== Listening =====================%

%Apply filter to sound files
path = '/Users/Morgan/Dropbox/CNAM/Memoire/Matlab/48k/filtered';
%---------Pink noise-----------%
%left - no filtered / right - filtered
[y] = audioread('/48k/Pink.wav');
yf = conv(y,ih0);
y = [y yf(1:size(y))];
file = fullfile(path,'Pink_filtered.wav');
audiowrite(file,y,Fs);

%------------Filtering songs-------------%
%%{
[y] = audioread('/48k/Pink Floyd -  I wish you were here.wav');
y1 = y(:,1);
y1 = conv(y1,ih0);
y2 = y(:,2);
y2 = conv(y2,ih0);
%Normalization if clipping
m = max(max(abs(y1)),max(abs(y2)));
if(m>1)
    y1 = y1 ./ m;
    y2 = y2 ./ m;
end
y=[y1 y2];
file = fullfile(path,'Pink Floyd -  I wish you were here_filtered.wav');
audiowrite(file,y,Fs);

%}








