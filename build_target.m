close all;


%%{
m_sub = 15;                       
m_lf = 10;
m_mf = 0;
m_hf = 0;

f_sub = 60;
f_lf = 120;
f_mf = 1000;
f_hf1 = 10000;
f_hf2 = 20000;

%{
F = zeros(size(f,1),1);

for i=1:size(f,1);
    % sub plateau
    if f(i) < f_sub
         F(i) = m_sub;
    
    % LF transition    
    elseif f(i) < f_lf
        F(i) = m_sub-(m_sub-m_lf)*log10(f(i)/f_sub)/log10(f_lf/f_sub);
    
    %MF transition
    elseif f(i) < f_mf
        F(i) = m_lf-(m_lf-m_mf)*log10(f(i)/f_lf)/log10(f_mf/f_lf);
    %MF plateau
    elseif f(i) < f_hf1
        F(i) = m_mf;
    
    % HF shelf  
    elseif
    end
end
%}

m = [ 15 15 10 0 0]
fq = [ 30 60 120 1000 10000]
F = 10.^(interp1(log(fq),m,log(f), 'linear')/20);

figure();
semilogx(f,20*log10(F));
axis([10 10000 -5 20]);
