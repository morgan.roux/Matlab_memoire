
clear;
close all;
samples = 1024; %450/1000*48000+1;     % Window Length of FFT 
Fs=48000;
t=(0:samples-1)/Fs;
Fmin = 30;
Fmax = 20000;
Mmin = -70;
Mmax = 10;
SmoothMag = 101;
SmoothPh = 5;
Fn = Fs/2;   
nfft = 2^nextpow2(samples);
f=(0:nfft/2-1)*Fs/nfft;
Fprecision = Fs/nfft;



[b,a] = ellip(6,5,40,0.6);
figure();
freqz(b,a);
imp = [1; zeros(samples,1)];
x = filter(b,a,imp);
X = fft(x);
Y = abs(fft(x));
y = fftshift(real(ifft(Y)));

Y2 = fft(y,nfft);

%max(abs(x-y))
figure();

b = firminphase(y);
b=b';


B = fft(b,nfft);
X=fft(x,nfft);
plot(1:size(B),20*log10(B),1:size(Y2),20*log10(Y2));
legend('B','X');


figure();
plot(1:size(x), x,1:size(y), y);
legend('x','y');