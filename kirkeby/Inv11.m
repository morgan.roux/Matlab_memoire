% Fast deconvolution of a single-channel system using regularisation
%
% The user can set the following parameters:
% 
% c    - the sequence that has to be deconvolved (column vector)
% Nh   - the number of coefficients in the inverse filter h
% leak - the regularisation parameter
%
% Ole Kirkeby, October 1995

if (~exist('Nh'))	Nh	= 1024;			end
if (~exist('leak'))	leak	=    0.001;		end

if (~exist('c'))  randn('seed',0.5); c=randn(32,1); end 

Nc=length(c);
C=fft(c,Nh);

H=conj(C)./(conj(C).*C+leak); %inverts C
h=fftshift(real(ifft(H))); %implements modelling delay
w=conv(c,h);

figure();
subplot(221); plot([0:Nh-1],h);    xlabel('n'); title('a) h(n)'); %axis([0 Nh    -0.2 0.2])
subplot(222); plot([0:Nh+Nc-2],w); xlabel('n'); title('b) w(n)'); axis([0 Nh+Nc -0.2 1.0])
subplot(223); plot(0.5 *[0:  Nh-1]/Nh,20*log10(abs(freqz(h,1,  Nh)))); xlabel('f/fs'); ylabel('dB');
              title('c) H(f)'); %axis([0 0.5 -40 20]);
subplot(224); plot(0.25*[0:2*Nh-1]/Nh,20*log10(abs(freqz(w,1,2*Nh)))); xlabel('f/fs'); ylabel('dB'); 
              title('d) W(f)'); axis([0 0.5 -40 10]);