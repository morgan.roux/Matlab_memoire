%Base FFT
clear; 
close all;

%[FileName,PathName] = uigetfile('*.*','s?lectionnez la mesure');

%Ouverture des impulses
%%{
filenames = {'top1.wmb';'top2.wmb' ; ...
              'top3.wmb' ; 'top4.wmb';...
              'top5.wmb' ; 'top6.wmb' ; ...
              'top7.wmb' ; 'top1.wmb';
  };


%{
filenames = {'SUB BOT.wmb';...
             'SUB TOP.wmb';...
             'MAIN BOT.wmb';...
             'MAIN TOP.wmb';};
%}   

%Chargement des impulses ? partir de winMLS
%%{
[imps,Fs, Format]=loadimp(char(filenames(1)));
 % [imps,Fs, Format]=loadimp(char(fullfile(PathName,FileName)));  
for k=2:length(filenames)
    imp=loadimp(char(filenames(k)));
    imps=[imps imp];
end
%}

%Chargement des impulses ? partir de fichiers wav.
%{
[imps,Fs]=wavread(char(filenames(1)));
    
for k=2:length(filenames)
    imp=wavread(char(filenames(k)));
    imps=[imps imp];
end
%}

h = imps;
%=========== Initialisation des variables ===============
%Nombre d'Impulses
nImp = length(filenames);

%Pour l'affichage
couleur = hsv(nImp+2);
legendeMag = cell(1,nImp);
legendeMes = cell(1,nImp/2);
for ii=1:(nImp/2)
    legendeMes(ii) = {sprintf('Mes %i',ii)};
end
for ii=1:nImp
   legendeMag(ii)=filenames(ii);
end
legendePh = legendeMag;



t0=(0:size(h,1)-1)/Fs;

samples = 450/1000*48000;     % Window Length of FFT 
t=(0:samples-1)/Fs;
Fmin = 30;
Fmax = 20000;
Mmin = -70;
Mmax = 10;
SmoothMag = 101;
SmoothPh = 5;

Fn = Fs/2;   
nfft = 2^nextpow2(samples);
f=(0:nfft/2-1)*Fs/nfft;
Fprecision = Fs/nfft;

offset = 0; %20/1000*Fs;        %en samples
winLen1= 20/1000*Fs*2;
winLen2 = 400/1000*Fs*2;

Phase = 0;              %0 : en phase, 1:oppo de phase


%Windowing
w1=hanning(winLen1);
w2=hanning(winLen2);
w=[w1(1:winLen1/2) ; ...
   ones(samples-winLen1/2-winLen2/2-1 ,1) ; ...
   w2(winLen2/2:end)];
   

h=h(1+offset:samples+offset,:);
hn=zeros(size(h));

for k=1:size(h,2);
    h(:,k) = h(:,k).*w;
    hn(:,k)=h(:,k)/max(abs(h(:,k)));
end

%wvtool(w);
%wvtool(hanning(samples));

%Calculs des fft
H=fft(h,nfft);
H=H(1:nfft/2,:);

%Calcul d'une moyenne
Hmoy = [ sum( abs(H(:,1:size(H,2))) ,2) / size(H,2) ];


% ================= Preprocessing =================%
%h0 = impulse repsonse to inverse

%single measurement
h0 = h(:,2)/max(h(:,2));

%Multiple measurment : RMS averaging
hmoy = ifft(Hmoy);
figure();
plot(hmoy);

%============== Inversing ==================%

%Kirkeby%
ih = invFIR('minphase',h0,nfft,3,1024,[32 16000], [30 -10],0);
IH = fft(ih,nfft);
IH = IH(1:nfft/2,:);

%convolution
r = conv(ih,h0);
r=r(1:samples,1);


%Calcul des fft
H0 = fft(h0,nfft);
H0 = H0(1:nfft/2,:);

HR = fft(r,nfft);
HR = HR(1:nfft/2,:);


%Affichage des FFT
figure('Name', 'kirkeby fft');
subplot(2,1,1);
semilogx(f,smooth(20*log10(H0),SmoothMag),...
    f,smooth(20*log10(IH),SmoothMag),...
    f, smooth(20*log10(HR),SmoothMag) );
legend('HO','IH','HR');
axis([Fmin Fmax -60 60]);
subplot(2,1,2);
semilogx( f, angle(IH));
legend('H0','IH','HR');


x=0:size(r)-1;
figure('Name','kirkeby');
subplot(2,1,1);
plot(t, h0/max(h0))
legend('ho');
subplot(2,1,2);
plot(t, r/max(r))
legend('r');


%=========== Affichage =============

%FFTs
figure('Name','Moyennes');

%Magnitude

%subplot(2,1,1);
semilogx(f,20*log10(smooth(abs(H(:,1)),SmoothMag)),...
             'color',couleur(1,:),'linewidth',1);

for k=2:size(H,2)
    hold on
    semilogx(f,20*log10(smooth(abs(H(:,k)),SmoothMag)),...
             'color',couleur(k,:),'linewidth',1);
end

%%{
hold on
semilogx(f,20*log10(smooth(abs(Hmoy(:,1)),SmoothMag)),...
        'color',couleur(end-1,:),'linewidth',2.5); %}  
axis([Fmin Fmax Mmin Mmax]);
axis 'auto y';

%On r?cup?re la bonne ?chelle de magnitude
v = axis;
Mmin = v(3);
Mmax = v(4)+5;
legend(legendeMag );






