%Base FFT
clear; 
close all;

%[FileName,PathName] = uigetfile('*.*','s?lectionnez la mesure');

%Ouverture des impulses
%%{
filenames = {'sub1.wmb' ; 'sub2.wmb' ; ...
              'sub3.wmb' ; 'sub4.wmb';...
              'top1.wmb';'top2.wmb' ; ...
              'top3.wmb' ; 'top4.wmb';
  };


%{
filenames = {'SUB BOT.wmb';...
             'SUB TOP.wmb';...
             'MAIN BOT.wmb';...
             'MAIN TOP.wmb';};
%}   

%Chargement des impulses ? partir de winMLS
%%{
[imps,Fs, Format]=loadimp(char(filenames(1)));
 % [imps,Fs, Format]=loadimp(char(fullfile(PathName,FileName)));  
for k=2:length(filenames)
    imp=loadimp(char(filenames(k)));
    imps=[imps imp];
end
%}

%Chargement des impulses ? partir de fichiers wav.
%{
[imps,Fs]=wavread(char(filenames(1)));
    
for k=2:length(filenames)
    imp=wavread(char(filenames(k)));
    imps=[imps imp];
end
%}

h = imps;
%=========== Initialisation des variables ===============
%Nombre d'Impulses
nImp = length(filenames);

%Pour l'affichage
couleur = hsv(nImp+2);
legendeMag = cell(1,nImp);
legendeMes = cell(1,nImp/2);
for ii=1:(nImp/2)
    legendeMes(ii) = {sprintf('Mes %i',ii)};
end
for ii=1:nImp
   legendeMag(ii)=filenames(ii);
end
legendePh = legendeMag;



t0=(0:size(h,1)-1)/Fs;

samples = 450/1000*48000;     % Window Length of FFT 
t=(0:samples-1)/Fs;
Fmin = 30;
Fmax = 20000;
Mmin = -70;
Mmax = 10;
SmoothMag = 7;
SmoothPh = 5;

Fn = Fs/2;   
nfft = 2^nextpow2(samples);
f=(0:nfft/2-1)*Fs/nfft;
Fprecision = Fs/nfft;

offset = 0; %20/1000*Fs;        %en samples
winLen1= 20/1000*Fs*2;
winLen2 = 400/1000*Fs*2;

Phase = 0;              %0 : en phase, 1:oppo de phase

%filenm = 'Phil COLLINS - Another day in paradise.wav';
%=========================================================

%Windowing
w1=hanning(winLen1);
w2=hanning(winLen2);
w=[w1(1:winLen1/2) ; ...
   ones(samples-winLen1/2-winLen2/2-1 ,1) ; ...
   w2(winLen2/2:end)];
   

h=h(1+offset:samples+offset,:);
hn=zeros(size(h));

for k=1:size(h,2);
    h(:,k) = h(:,k).*w;
    hn(:,k)=h(:,k)/max(abs(h(:,k)));
end

%wvtool(w);
%wvtool(hanning(samples));

%Calculs des fft et affichage
H=fft(h,nfft);
H=H(1:nfft/2,:);

%Calcul d'une moyenne
Hmoy0 = [ sum( abs(H(:,1:(size(H,2)/2))) ,2) / (size(H,2)/2) ... %moyenne des subs
        sum( abs(H(:,(size(H,2)/2+1):end)) ,2) / (size(H,2)/2) ];%moyennes des mains

%FFTs
figure('Name','Moyennes');

%Magnitude

%subplot(2,1,1);
semilogx(f,20*log10(smooth(abs(H(:,1)),SmoothMag)),...
             'color',couleur(1,:),'linewidth',1);

for k=2:size(H,2)
    hold on
    semilogx(f,20*log10(smooth(abs(H(:,k)),SmoothMag)),...
             'color',couleur(k,:),'linewidth',1);
end

%%{
hold on
semilogx(f,20*log10(smooth(abs(Hmoy0(:,1)),SmoothMag)),...
        'color',couleur(end-1,:),'linewidth',2.5);
hold on
semilogx(f,20*log10(smooth(abs(Hmoy0(:,2)),SmoothMag)),...
        'color',couleur(end,:),'linewidth',2.5);
 %}  
axis([Fmin Fmax Mmin Mmax]);
axis 'auto y';

%On r?cup?re la bonne ?chelle de magnitude
v = axis;
Mmin = v(3);
Mmax = v(4)+5;
legend(legendeMag );

%Phase
%%{
figure('Name','Phase');

semilogx(f,radtodeg(angle(smooth(H(:,1),SmoothPh))),...
         'color',couleur(1,:),'linewidth',1);
%{     
for k=2:size(H,2)
    hold on
    semilogx(f,radtodeg(angle(smooth(H(:,k),SmoothPh))),...
             'color',couleur(k,:),'linewidth',1);
end
  %}   
axis([Fmin Fmax -180 180]);
legend(legendePh);
%}

%================ Affichage de la sommation=================% 
%================= en chaque point sans modif ===============%
%=================         du delay         ==================%

S = zeros(size(H,1),nImp/2);
for kk=1:nImp/2
    S(:,kk)=H(:,kk)+H(:,kk+nImp/2);
end


%Affichage

%Magnitude
figure('Name',sprintf('Magnitude, delay = 0'));

for ii=1:nImp/2
    
    
    subplot(ceil(nImp/6),3,ii);
    semilogx(f,20*log10(smooth(abs(S(:,ii)),SmoothMag)),...
            f,20*log10(smooth(abs(H(:,ii)),SmoothMag)),...
            f,20*log10(smooth(abs(H(:,ii+nImp/2)),SmoothMag)));
    axis([Fmin Fmax Mmin Mmax]);
    title(sprintf('Mesure %i',ii));
    legend('Sum','Sub','Main');
    
end

figure('Name','Ris normalis?es, delay = 0');
for ii=1:nImp/2
    
    subplot(ceil(nImp/6),3,ii);
    plot(t, h(:,ii)/abs(max(h(:,ii))),...
         t,h(:,ii+nImp/2)/abs(max(h(:,ii+nImp/2))),...
         t,w);
    title(sprintf('Mesure %i',ii));
    legend('Sub','Main','Window');
end

%sauvegarde des impulses et des fft
h0 = h;
H0 = H;
S0 = S;


%==================== Calcul du delay ==================%
%================= de chaque point de mesures ==========%

%            Maximiser la RI sans opposition de phase
%                    -> Xcorrelation

%=========================================================%
%=========================================================%

%Initialisation
delta = zeros(1,nImp/2);
xcorMax = zeros(1,nImp/2);
R = zeros(samples*2-1,nImp/2);

%Calcul Cross correlation et calcul des retards
figure('Name','Cross Correlation');
for ii=1:nImp/2
    
    %lags>0 => les subs sont en retard
    %lags<0 => les subs sont en avance
    
    [r,lags] = xcorr(h(:,ii),h(:,ii+nImp/2),'coeff');
    [m,i] = max(r);
    
    %On stocke le calcul
    R(:,ii) = r;
    
    delta(ii) = lags(i);
    xcorMax(ii) = r(i);
    
    %Affichage XCorr avec maxima
    [pks,locs]=findpeaks(r);
    [mpk,mloc] = max(pks);
    
    subplot(ceil(nImp/6),3,ii);
    plot(lags,r,lags(locs(mloc)),r(locs(mloc)),'or');
    title(sprintf('delta = %i samples',delta(ii)));
    
end

%Application du retard et de l'oppo de phase
for ii=1:nImp/2
    %Oppo de phase ?
    %if xcorMax(ii) < 0
     %h(:,ii) = -h(:;ii);  
     %H(:,ii) = H(:,ii) .+ pi;
    %end

    %Compensation  du delay sur le main (positif ou n?gatif)
    H(:,ii+nImp/2) = H(:,ii+nImp/2) .* exp(-2*pi*1i*delta(ii)/Fs*f');
    
    if delta(ii) >= 0
        %Sub en retard -> On retarde le main    
        h(:,ii+nImp/2) = [zeros(delta(ii),1); ...
                         h(1:(size(h,1)-delta(ii)),ii+nImp/2)];
                      
    else
        %T?tes en retard -> on retard le sub
         h(:,ii) = [zeros(-delta(ii),1); ...
                    h(1:(size(h,1)+delta(ii)),ii)];
    end
    %}
end

%{

%Sommation
s = zeros(size(h,1),nImp/2);
for ii=1:nImp/2 
    s(:,ii) = h(:,ii)+h(:,ii+nImp/2);
end

%Calcul FFT
H=fft(h,nfft);
H=H(1:nfft/2,:);

S=fft(s,nfft);
S=S(1:nfft/2,:);

%Correction de phase pour l'affichage
[m,retard] = max(s,[],1);
Hd = exp((2*pi*1i*f')*(retard/Fs));
%}

%sommation
S = zeros(size(H,1),nImp/2);
for kk=1:nImp/2
    S(:,kk)=H(:,kk)+H(:,kk+nImp/2);
end


%Affichage

%Magnitude
figure('Name', 'Magnitude apr?s cross correlation');
for ii=1:nImp/2
    
    
    subplot(ceil(nImp/6),3,ii);
    semilogx(f,20*log10(smooth(abs(S(:,ii)),SmoothMag)),...
            f,20*log10(smooth(abs(H(:,ii)),SmoothMag)),...
            f,20*log10(smooth(abs(H(:,ii+nImp/2)),SmoothMag)));
    axis([Fmin Fmax Mmin Mmax]);
    title(sprintf('delta =  =  %.3g ms',1000*delta(ii)/Fs));
    
  %  legend(sprintf('sum%i',ii),...
   %        sprintf('sub%i',ii),...
    %       sprintf('main%i',ii));
end

%Phase
%{
figure;
for ii=1:nImp/2
   
   % subplot(ceil(nImp/6),3,ii);
   subplot(2,2,ii); 
   semilogx(f,radtodeg(angle(smooth(S(:,ii).*Hd(:,ii),SmoothPh))),...
             f,radtodeg(angle(smooth(H(:,ii).*Hd(:,ii),SmoothPh))),...
             f,radtodeg(angle(smooth(H(:,ii+nImp/2).*Hd(:,ii),SmoothPh))));
    axis([Fmin Fmax -180 180]);
    legend(sprintf('sum%i',ii),...
           sprintf('sub%i',ii),...
           sprintf('main%i',ii));
end
%}
%RIs normalis?es
%{
figure;
for ii=1:nImp/2
    
    subplot(ceil(nImp/6),3,ii);
    plot(t, h(:,ii)/abs(max(h(:,ii))),...
         t,h(:,ii+nImp/2)/abs(max(h(:,ii+nImp/2))));
    legend(sprintf('sub%i',ii),...
           sprintf('main%i',ii));
end
%}
%RIs apr?s sommations
%{
figure;
for ii=1:nImp/2
    
    subplot(ceil(nImp/6),3,ii);
    plot(t, s(:,ii));
    legend(sprintf('sum%i',ii));
end
%}


%================= Calcul du d?lai moyen ==================%
%==========================================================%

%Restauration des impulses
h=h0;
H=H0;


%--------------------- M?thode 1  -----------------------%
%       Pond?ration du d?lai par la cross corr?lation

%{
Calcul du delay
deltaMoy = round(sum(delta.*xcorMax)/sum(xcorMax));
%}

%--------------------------------------------------------%
%--------------------- M?thode 2 ------------------------%
%          Pond?ration du d?lai par l'utilisateur

%{
Calcul du delay
%pond=[1 1 1];
%deltaMoy = round(sum(delta.*pond)/sum(pond));
%}

%--------------------------------------------------------%
%--------------------- M?thode 3 ------------------------%
%           Optimisation de la somme des magnitudes
%           avec pond?ration des points par 
%                      l'utilisateur                     %

%{

Tprecision =round(0.001*Fs);  %pr?cision de la recherche en nbr de sample
TSpanMin = min(delta);
TSpanMax = max(delta);
FSpanMin = round(Fmin*nfft/Fs);
FSpanMax = round(Fmax*nfft/Fs);

TSpan=TSpanMin:Tprecision:TSpanMax;
FSpan=FSpanMin:FSpanMax; %1:size(H,1);%

pond=[1 1 1 1 1 1 1 1];

Somme=zeros(size(TSpan));
   
%Pour chaque d?calage temporel (TSpan), on somme toutes les magnitudes
%des sommations en chaque point.

for jj=1:size(TSpan,2)
    %pour chaque d?calage temporel TSpan(jj)
    for kk=1:nImp/2
        %pour chaque point de mesure kk

        %On somme toutes les composantes fr?quentielle de la mesure
        A = abs(H(FSpan,kk)...
            + H(FSpan,kk+nImp/2).*exp(-2*pi*1i*TSpan(jj)/Fs*f(FSpan)'));

        %Et on les ajoute aux autres point de mesures avec une pond?ration
        Somme(jj) = Somme(jj)+pond(kk)*sum(A);
    end
end

%On cherche le delay optimal
[m,iOpt] = max(Somme);
deltaMoy = TSpan(iOpt);

%}

%--------------------------------------------------------%
%--------------------- M?thode 4 ------------------------%
%           Optimisation de l'?cart type 
%                 des magnitudes

%{


Tprecision =round(0.001*Fs);  %pr?cision de la recherche en nbr de sample
TSpanMin = min(delta);
TSpanMax = max(delta);
FSpanMin = round(Fmin*nfft/Fs);
FSpanMax = round(Fmax*nfft/Fs);

TSpan=TSpanMin:Tprecision:TSpanMax;
FSpan=FSpanMin:FSpanMax; %1:size(H,1);%

pond=[1 1 1 1 1 1 1 1];

%Ecarts(jj,kk) contient l'?cart type des magnitudes
%de la sommation sub + main d?cal? de TSpan(kk) au point de mesure jj
Ecarts=zeros(nImp/2,size(TSpan,2));
  

for jj=1:nImp/2
    %pour chaque point de mesure jj
    
    for kk=1:size(TSpan,2)
    %pour chaque d?calage temporel TSpan(kk)
    
        %A(Fspan,1) contient toute les magnitudes 
        %du r?sultat de la somme au point de mesure jj
        %entre le sub et le main d?cal? de TSpan(kk)
        
      
        A = abs(H(FSpan,jj)+ ...
            H(FSpan,jj+nImp/2).*exp(-2*pi*1i*TSpan(kk)/Fs*f(FSpan)'));
        
        Ecarts(jj,kk) = pond(jj)*std(A);
    end
end

%On calcule la somme des ?cart type de chaque point de mesure, 
%pour chaque d?calage ind?pendamment

%On cherche le delay optimal
[m,iOpt] = min(sum(Ecarts));
deltaMoy = TSpan(iOpt);

titre = cell(1,nImp/2);
for ii=1:nImp/2
    titre(ii) = {sprintf('?cart type = %.3g',Ecarts(ii,iOpt))};
end

%}

%--------------------------------------------------------%
%--------------------- M?thode 4BIS ---------------------%
%               Optimisation de l'?cart type 
%                     des magnitudes
%                     CALCUL EN DB

%{

Tprecision = round(0.001*Fs);  %pr?cision de la recherche en nbr de sample
TSpanMin = min(delta);
TSpanMax = max(delta);
FSpanMin = round(Fmin*nfft/Fs);
FSpanMax = round(Fmax*nfft/Fs);

TSpan=TSpanMin:Tprecision:TSpanMax;
FSpan=FSpanMin:FSpanMax; %1:size(H,1);%

pond=[1 1 1 1 1 1 1 1];

%Ecarts(jj,kk) contient l'?cart type des magnitudes
%de la sommation sub + main d?cal? de TSpan(kk) au point de mesure jj
Ecarts=zeros(nImp/2,size(TSpan,2));
  

for jj=1:nImp/2
    %pour chaque point de mesure kk
    
    for kk=1:size(TSpan,2)
    %pour chaque d?calage temporel TSpan(jj)
    
        %S(Fspan,1) contient toute les magnitudes EN DB
        %du r?sultat de la somme au point de mesure jj
        %entre le sub et le main d?cal? de TSpan(kk)
        
      
        S = 20*log10(abs(H(FSpan,jj)+ ...
            H(FSpan,jj+nImp/2).*exp(-2*pi*1i*TSpan(kk)/Fs*f(FSpan)')));
        
        Ecarts(jj,kk) = pond(jj)*std(S);
    end
end

%On calcule la somme des ?cart type de chaque point de mesure, 
%pour chaque d?calage ind?pendamment

%On cherche le delay optimal
[m,iOpt] = min(sum(Ecarts));
deltaMoy = TSpan(iOpt);

%Titre pour l'affichage
titre = cell(1,nImp/2);
for ii=1:nImp/2
    titre(ii) = {sprintf('?cart type = %.3g dB',Ecarts(ii,iOpt))};
end

%}

%--------------------------------------------------------%
%--------------------- M?thode 5 ------------------------%
%              Optimisation de l'?cart moyen
%             des magnitudes par rapport ? la
%                       moyenne

%{
Tprecision = round(0.001*Fs);  %pr?cision de la recherche en nbr de sample
TSpanMin = min(delta);
TSpanMax = max(delta);
FSpanMin = round(Fmin*nfft/Fs);
FSpanMax = round(Fmax*nfft/Fs);

TSpan=TSpanMin:Tprecision:TSpanMax;
FSpan=FSpanMin:FSpanMax; %1:size(H,1);%

pond=[1 1 1 1 1 1 1 1];

%Ecart(kk) contient la somme des ?carts moyen des courbes
%en chaque point de mesure par rapport ? la courbe moyenne 
%pour un d?calage TSpan(kk)

Ecarts=zeros(nImp/2,size(TSpan,2));

for kk=1:size(TSpan,2)
%pour chaque d?calage temporel TSpan(kk)
    
    %Calcul de la sommation sub+(main d?cal? de Tspan(kk))
    %pour chaque point de mesure
    S = zeros(size(H,1),nImp/2);
    for jj=1:nImp/2
        S(:,jj)=H(:,jj)+H(:,jj+nImp/2).* exp(-2*pi*1i*TSpan(kk)/Fs*f');
    end
    
    %Calcul de la courbe moyenne pour un d?calage de TSpan(kk)
    Moy(:,kk) = mean(abs(S),2);
    
    for jj=1:nImp/2
        %pour chaque point de mesure jj
    
        %Calcul de la moyenne des ?cart absolus de la sommation au point jj 
        %par rapport ? la moyenne (on ne calcule que les magnitude)
        %Pour un d?calage Tspan(kk)
        Ecarts(jj,kk) = mean(abs(abs(S(:,jj)) - abs(Moy(:,kk))));
       
    end
end
%On somme les ecarts de chaque point de mesure pour un d?calage donn?
%et on cherche le delay optimal
[m,iOpt] = min(sum(Ecarts,1));
deltaMoy = TSpan(iOpt);

titre = cell(1,nImp/2);
for ii=1:nImp/2
    titre(ii) = {sprintf('?cart moyen = %.3g',Ecarts(ii,iOpt))};
end
%}

%--------------------------------------------------------%
%--------------------- M?thode 5BIS ------------------------%
%              Optimisation de l'?cart moyen
%             des magnitudes par rapport ? la
%                       moyenne
%                      CALCUL EN DB

%{

Tprecision =round(0.0001*Fs);  %pr?cision de la recherche en nbr de sample
TSpanMin = min(delta);
TSpanMax = max(delta);
FSpanMin = round(Fmin*nfft/Fs);
FSpanMax = round(Fmax*nfft/Fs);

TSpan=TSpanMin:Tprecision:TSpanMax;
FSpan=FSpanMin:FSpanMax; %1:size(H,1);%

pond=[1 1 1 1 1 1 1 1];

%Ecart(kk) contient la somme des ?carts moyen des courbes
%en chaque point de mesure par rapport ? la courbe moyenne 
%pour un d?calage TSpan(kk)

Ecarts=zeros(nImp/2,size(TSpan,2));
Moy = zeros(size(H,1),size(TSpan,2));

for kk=1:size(TSpan,2)
%pour chaque d?calage temporel TSpan(kk)
    
    %Calcul de la sommation sub+(main d?cal? de Tspan(kk))
    %pour chaque point de mesure
    S = zeros(size(H,1),nImp/2);
    for jj=1:nImp/2
        S(:,jj)=H(:,jj)+H(:,jj+nImp/2).* exp(-2*pi*1i*TSpan(kk)/Fs*f');
    end
    
    %Calcul de la courbe moyenne pour un d?calage de TSpan(kk)
    Moy(:,kk) = mean(abs(S),2);
    
    for jj=1:nImp/2
        %pour chaque point de mesure jj
    
        %Calcul de la moyenne des ?cart absolus de la sommation au point jj 
        %par rapport ? la moyenne (on ne calcule que les magnitude)
        %Pour un d?calage Tspan(kk)
        Ecarts(jj,kk) = mean(abs(20*log10(abs(S(:,jj))) ...
                              - 20*log10(abs(Moy(:,kk)))));
       
    end
end
%On somme les ecarts de chaque point de mesure pour un d?calage donn?
%et on cherche le delay optimal
[m,iOpt] = min(sum(Ecarts,1));
deltaMoy = TSpan(iOpt);

titre = cell(1,nImp/2);
for ii=1:nImp/2
    titre(ii) = {sprintf('?cart moyen = %.3g dB',Ecarts(ii,iOpt))};
end
%}

%--------------------------------------------------------%
%--------------------- M?thode 6 ------------------------%
%            Optimisation de la moyenne des
%                  cross correlation

%%{
%On moyenne les cross correlation pour chaque impulse
%et on cherche le delai optimal

Rmoy = mean(R,2);
[m,iOpt] = max(Rmoy);
deltaMoy = lags(iOpt);

%Affichage de la moyenne de la cross corr
figure('Name','Cross correlation moyenne');
plot(lags,Rmoy,lags(iOpt),Rmoy(iOpt),'or');
    title(sprintf('delta =  %i samples',lags(iOpt)));


titre = cell(1,nImp/2);
for ii=1:nImp/2
    titre(ii) = {sprintf('Cross correlation = %.3g ',R(iOpt,ii))};
end

%}

%-------------------------------------------------------%
%-------------------Simulation du retard ---------------%

%Application du retard et de l'oppo de phase
for ii=1:nImp/2
    %Application du delay sur le main (positif ou n?gatif)
    H(:,ii+nImp/2) = H(:,ii+nImp/2) .* exp(-2*pi*1i*deltaMoy/Fs*f');
    
    %Application de l'oppo de phase sur le sub
    H(:,ii) = H(:,ii) .*exp(-pi*1i*Phase);
    
    %D?calage des Ris
    if deltaMoy >= 0
        %Sub en retard -> On retarde le main    
        h(:,ii+nImp/2) = [zeros(deltaMoy,1); ...
                          h(1:(size(h,1)-deltaMoy),ii+nImp/2)];
                      
    else
        %T?tes en retard -> on retard le sub
         h(:,ii) = [zeros(-deltaMoy,1); ...
                    h(1:(size(h,1)+deltaMoy),ii)];
    end
    
    %Oppo de phase sur le sub
    if Phase == 1
        h(:,ii) = - h(:,ii);
    end
    
    
end

%Calcul des sommations
S = zeros(size(H,1),nImp/2);
for kk=1:nImp/2
    S(:,kk)=H(:,kk)+H(:,kk+nImp/2);
end


%Calcul des moyennes
MagMoy0 = sum( abs(S0) ,2) / (nImp/2);      %moyenne avant
MagMoy = sum( abs(S),2) / (nImp/2);         %moyenne apr?s

%Affichage

%Magnitude
figure('Name',sprintf('Magnitude , delay = %d samples = %.3g ms @ %dHz',...
                       deltaMoy, 1000*deltaMoy/Fs, Fs));

for ii=1:nImp/2
    
    
    subplot(ceil(nImp/6),3,ii);
    semilogx(f,20*log10(smooth(abs(S(:,ii)),SmoothMag)),...
            f,20*log10(smooth(abs(H(:,ii)),SmoothMag)),...
            f,20*log10(smooth(abs(H(:,ii+nImp/2)),SmoothMag)));
   
    %hold on
    %semilogx(f,20*log10(smooth(MagMoy,SmoothMag)),...
    %         'linewidth',2.5);    

    axis([Fmin Fmax Mmin Mmax]);
    
    
    title(titre(ii));
end

%Sommation
s = zeros(size(h,1),nImp/2);
for ii=1:nImp/2 
    s(:,ii) = h(:,ii)+h(:,ii+nImp/2);
end

%Calcul des moyennes
MagMoy0 = sum( abs(S0) ,2) / (nImp/2);      %moyenne avant
MagMoy = sum( abs(S),2) / (nImp/2);         %moyenne apr?s

%Correction de phase pour l'affichage
[m,retard] = max(s,[],1);
Hd = exp((2*pi*1i*f')*(retard/Fs));

%Phase
%{



figure;
for ii=1:nImp/2
   
   % subplot(ceil(nImp/6),3,ii);
   subplot(2,2,ii); 
   semilogx(f,radtodeg(angle(smooth(S(:,ii).*Hd(:,ii),SmoothPh))),...
             f,radtodeg(angle(smooth(H(:,ii).*Hd(:,ii),SmoothPh))),...
             f,radtodeg(angle(smooth(H(:,ii+nImp/2).*Hd(:,ii),SmoothPh))));
    axis([Fmin Fmax -180 180]);
    legend(sprintf('sum%i',ii),...
           sprintf('sub%i',ii),...
           sprintf('main%i',ii));
end
%}
%RIs normalis?es

figure('Name','Ris normalis?es apr?s optimisation');
for ii=1:nImp/2
    
    subplot(ceil(nImp/6),3,ii);
    plot(t, h(:,ii)/abs(max(h(:,ii))),...
         t,h(:,ii+nImp/2)/abs(max(h(:,ii+nImp/2))));
    title(sprintf('Mesure %i',ii));
end


for ii=1
    figure('Name','EDT apr?s optimisation');
   
    plot(t, log((h(:,ii)/abs(max(h(:,ii)))).^2),...
         t,log((h(:,ii+nImp/2)/abs(max(h(:,ii+nImp/2)))).^2));
    title(sprintf('Mesure %i',ii));
   
end

%RIs apr?s sommations
%{
figure;
for ii=1:nImp/2
    
    subplot(ceil(nImp/6),3,ii);
    plot(t, s(:,ii));
    legend(sprintf('sum%i',ii));
end
%}

%Comparaison avant-apr?s
%FFTs

figure('Name','Avant optimisation');
%Magnitude

semilogx(f,20*log10(smooth(abs(S0(:,1)),SmoothMag)),...
             'color',couleur(1,:),'linewidth',1);

for k=2:nImp/2
    hold on
    semilogx(f,20*log10(smooth(abs(S0(:,k)),SmoothMag)),...
             'color',couleur(k,:),'linewidth',1);
end

hold on
semilogx(f,20*log10(smooth(MagMoy0,SmoothMag)),...
         'linewidth',2.5);    
axis([Fmin Fmax Mmin Mmax]);
%legend([legendeMes 'Moy avant'])


figure('Name','Apr?s optimisation');
%Magnitude

semilogx(f,20*log10(smooth(abs(S(:,1)),SmoothMag)),...
             'color',couleur(1,:),'linewidth',1);

for k=2:nImp/2
    hold on
    semilogx(f,20*log10(smooth(abs(S(:,k)),SmoothMag)),...
             'color',couleur(k,:),'linewidth',1);
end

hold on
semilogx(f,20*log10(smooth(MagMoy,SmoothMag)),...
         'linewidth',2.5);    
axis([Fmin Fmax Mmin Mmax]);
%legend([legendeMes 'Moy apr?s'])