clear;
close all;

samples = 2^16; %450/1000*48000+1;     % Window Length of FFT 
Fs=48000;
t=(0:samples-1)/Fs;
Fmin = 30;
Fmax = 20000;
Mmin = -70;
Mmax = 10;
SmoothMag = 101;
SmoothPh = 5;
Fn = Fs/2;   
nfft = 2^nextpow2(samples);
f=(0:nfft/2-1)*Fs/nfft;
Fprecision = Fs/nfft;


imp = [1 ; zeros(samples-1,1)];
freqz(imp);

%H = fft(imp);
H = 10.^[1:nfft/2  nfft/2:1];
H=H';
imp2 = real(ifft(abs(H),samples));
plot(t,imp,t,imp2);
legend('imp','imp2');